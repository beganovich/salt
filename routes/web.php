<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Authentication routes. 
Route::prefix('auth')
    ->group(function() {
        Route::get('register', 'Auth\RegisterController@index')->name('register.form');
        Route::post('register', 'Auth\RegisterController@store')->name('register.store');
        Route::get('login', 'Auth\LoginController@index')->name('login.form');
        Route::post('login', 'Auth\LoginController@attempt')->name('login.attempt');
        Route::get('logout', 'Auth\LogoutController@logout')->name('logout');
    });

// Dashboard routes.
Route::prefix('dashboard')
    ->group(function() {
        Route::get('/', 'User\DashboardController@index')->name('user.dashboard');
    });

// Group routes
Route::prefix('group')
    ->group(function() {
        Route::post('store', 'User\GroupController@store')->name('group.store');
    });

// Role routes
Route::prefix('role')
    ->group(function() {
        Route::post('edit', 'User\RoleController@edit')->name('role.edit');
        Route::post('create', 'User\RoleController@create')->name('role.create');
        Route::get('delete/{name}', 'User\RoleController@delete')->name('role.delete');
    });

Route::prefix('member')
    ->group(function() {
        Route::post('role', 'User\MemberController@role')->name('member.role');
    });
