<!DOCTYPE html>
<html>

<head>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="/css/materialize.min.css" media="screen,projection" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>SALT - @yield('title')</title>
</head>

<div class="navbar-fixed" style="margin-bottom: 80px">
<nav class="nav-extended blue darken-3">
		<div class="nav-wrapper container">
			<a href="{{ route('user.dashboard') }}" class="brand-logo">SALT</a>
			<a href="#" data-target="mobile-demo" class="sidenav-trigger" id="trigger">
				<i class="material-icons">menu</i>
			</a>

			<ul id="nav-mobile" class="right">
				<li class="hide-on-med-and-down">
					<a class="waves-effect waves-light z-depth-0 blue darken-3" data-target="mobile-demo" id="sidebtn">
                    <i class="material-icons right">menu</i>Howdy, {{ Auth::user()->name }}</a>
				</li>
			</ul>
		</div>

		<div class="nav-content container">
			<ul class="tabs tabs-transparent" id="tabs">
				
				<li class="tab">
					<a href="#groups">Groups</a>
				</li>

				<li class="tab">
					<a href="#users">Users</a>
				</li>

				<li class="tab">
					<a href="#roles">Roles</a>
				</li>

				<li class="tab">
					<a href="#settings">Settings</a>
				</li>
			</ul>
		</div>
	</nav>
</div>

	<ul class="sidenav" id="mobile-demo">

		<li>
			<div class="user-view">
				<div class="background">
					<img src="https://via.placeholder.com/500x500/0d47a1/000000">
				</div>
				<a href="#user">
					<img class="circle" src="https://via.placeholder.com/100x100/">
				</a>
				<a href="#name">
                <span class="white-text name">{{ Auth::user()->name }}</span>
				</a>
				<a href="#email">
                <span class="white-text email">{{ Auth::user()->email }}</span>
				</a>
			</div>
		</li>
		<li>
			<a href="">
				<i class="material-icons">settings</i>Settings
			</a>
		</li>
		<li>
        <a href="{{ route('logout') }}">
				<i class="material-icons">exit_to_app
				</i>Log out
			</a>
		</li>
	</ul>


<body>
    @yield('content')
</body>

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="/js/materialize.min.js"></script>
<script src="/js/dashboard.js"></script>

</html>
