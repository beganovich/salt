@extends('layouts.authentication') 
@section('title', 'Registration') 

@section('form')

<div class="container">
    <div class="row" style="margin-top: 8vh">

        <div class="col s12 m10 offset-m1 l4 offset-l4">
            
            @if ($errors->any())
            <div class="card red">
                <div class="card-content white-text center">
                    @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                    @endforeach
                </div>
            </div>
            @endif

            <div class="card darken-1">
                <div class="card-content black-text">
                    <span class="card-title center">Register</span>
                    <form action="{{ route('register.store') }}" method="post">

                        @csrf

                        <div class="input-field">
                            <input type="text" name="name" autofocus>
                            <span class="helper-text">Name</span>
                        </div>

                        <div class="input-field">
                            <input type="text" name="email">
                            <span class="helper-text">E-mail account</span>
                        </div>

                        <div class="input-field">
                            <input type="text" name="phone">
                            <span class="helper-text">Phone (+1 will be added itself)</span>
                        </div>

                        <div class="input-field">
                            <input type="password" name="password">
                            <span class="helper-text">Password</span>
                        </div>

                        <div class="input-field">
                            <input type="password" name="password_confirmation">
                            <span class="helper-text">Type your password again</span>
                        </div>

                </div>

                <div class="card-action blue darken-3 center-align">
                    <button type="submit" class="blue darken-3 waves-effect waves-dark btn z-depth-0" style="width: 100%">Register</button>
                </div>

                </form>

            </div>

            <div class="center-align">
                <a href="{{ route('login.form') }}">Existing user? Sign in</a>
            </div>

        </div>
    </div>
</div>

@endsection
