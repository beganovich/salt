@extends('layouts.authentication')
@section('title', 'Log in')

@section('form')

<div class="container">
    <div class="row" style="margin-top: 8vh">
        <div class="col s12 m10 offset-m1 l4 offset-l4">

            @if (session('error'))
            <div class="card red">
                <div class="card-content white-text center">
                    <p>{{ session('error') }}</p>
                </div>
            </div>
            @endif

            <div class="card darken-1">
                <div class="card-content black-text">
                    <span class="card-title center">To proceed, please log in.</span>
                    <br>
                    <form action="{{ route('login.attempt') }}" method="post">
                        @csrf

                        <div class="input-field">
                            <input type="email" name="email" autofocus>
                            <span class="helper-text">E-mail address</span>
                        </div>

                        <div class="input-field">
                            <input type="password" name="password">
                            <span class="helper-text">Password</span>
                        </div>

                </div>

                <div class="card-action blue darken-3 center-align">
                    <button type="submit" class="blue darken-3 waves-effect waves-light btn z-depth-0" style="width: 100%">Log in</button>
                </div>

                </form>

            </div>

            <div class="center-align">
                <a href="{{ route('register.form') }}">First time here? Make a new account!</a>
            </div>

        </div>
    </div>
</div>

@endsection