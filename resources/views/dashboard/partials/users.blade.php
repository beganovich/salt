<div class="row">
    <div class="col s12 m6 l7">
        <ul class="collection">

            @if(Auth::user()->group) @foreach (Auth::user()->group->member as $member)
            <li class="collection-item avatar">
                <img src="https://via.placeholder.com/500x500" alt="" class="circle">
                <span class="title">{{ App\User::find($member->id)->name }}
                    <span class="badge green white-text">{{ App\MemberRole::find(1)->role_id }}</span>
                </span>
                <p>E-mail: {{ App\User::find($member->id)->email }}
                    <br> Phone: {{ App\User::find($member->id)->phone }}
                </p>
            </li>
            @endforeach

        </ul>
    </div>

    <div class="col s12 m6 l5">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Assign role</span>
                <form action="{{ route('member.role')}}" method="post">
                @csrf
                <div class="input-field">
                    <select name="member">
                        <option disabled selected>Choose user</option>
                        @foreach(Auth::user()->group->member as $member)
                        <option value="{{ $member->id }}" class="blue-text">{{ App\User::find($member->id)->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="input-field">
                    <select name="role">
                        <option disabled selected>Choose role</option>
                        @foreach(Auth::user()->group->role as $role)
                        <option value="{{ $role->name }}" class="blue-text">{{ $role->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="card-action center-align white-text blue darken-3">
                <button type="submit" class="btn blue darken-3 z-depth-0 white-text">Assign</button>
            </div>
        </form>
        </div>
    </div>

</div>

@else
<p class="center-align">To manage users of the group, please create a group first.</p>
@endif
