<div class="row">

    @if(Auth::user()->group)
    <div class="col s12 m6 l4">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Roles list (
                    <b>{{ Auth::user()->group->name }}</b>)</span>
                <ul class="collection">

                    @foreach(Auth::user()->group->role as $role)
                        <li class="collection-item"><div>{{ $role->name }}<a href="{{ route('role.delete', ['name' => $role->name]) }}" class="secondary-content"><i class="material-icons">delete</i></a></div></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

    <div class="col s12 m6 l4">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Add role</span>
                <form action="{{ route('role.create') }}" method="post">
                    @csrf

                    <div class="input-field">
                        <input type="text" name="name" placeholder="Role name">
                    </div>
            </div>
            <div class="card-action center-align blue darken-3 white-text">
                <button type="submit" class="btn blue darken-3 white-text z-depth-0" style="width: 100%">Add role</button>
            </div>
            </form>
        </div>
    </div>

    <div class="col s12 m6 l4">
        <div class="card">
            <div class="card-content">
                <span class="card-title">Modify role</span>
                <form action="{{ route('role.edit') }}" method="post">
                    @csrf
                    <div class="input-field">
                        <select name="role">
                            <option disabled selected>Choose role</option>
                            @foreach(Auth::user()->group->role as $role)
                            <option value="{{ $role->name }}" class="blue-text">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="input-field">
                        <input type="text" name="value" placeholder="New role name">
                    </div>
            </div>
            <div class="card-action center-align blue darken-3 white-text">
                <button type="submit" class="btn blue darken-3 white-text z-depth-0" style="width: 100%">Modify role</button>
            </div>
            </form>
        </div>
    </div>
    @else
    <p>You can't manage roles, since you don't own a group.</p>
    @endif

</div>
