<div class="row" style="margin-top: 30px">
    @if(Auth::user()->group)
    <div class="col s12 m6 l4">
        <div class="card white darken-3">
            <div class="card-content blue-text">
                <span class="card-title">{{ Auth::user()->group->name }}  <span class="badge blue white-text">Creator</span></span>
                <p>{{ Auth::user()->group->description }}</p>
            </div>
        </div>
    </div>
    @else
    <div class="col s12 m6 l4">
        <div class="card blue darken-3">
            <div class="card-content white-text">
                <span class="card-title">New group</span>
                <p>Create your own group.</p>
            </div>
            <div class="card-action white center">
                <button id="createGroup" data-target="modal1" class="btn btn-small modal-trigger blue-text white z-depth-0">
                    <i class="material-icons left">add</i> Create</button>
            </div>
        </div>
    </div>
    @endif

    @include('dashboard.partials.includes.modal')

</div>
