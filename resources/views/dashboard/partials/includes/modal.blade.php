<div id="modal1" class="modal bottom-sheet">
    <div class="modal-content center">
        <h4>Create a group</h4>
        <div class="container">
            <div class="row">
                <div class="col s12 m4 l4 offset-l4">
								<form action="{{ route('group.store') }}" method="post">
                        @csrf

                        <div class="input-field">
                            <input type="text" name="group_name" autofocus>
                            <span class="helper-text">Group name</span>
                        </div>
                        <div class="input-field">
                            <textarea name="group_description" class="materialize-textarea"></textarea>
                            <span class="helper-text">Short description</span>
                        </div>
                        <button type="submit" class="waves-effect waves-light btn blue darken-3 z-depth-0">Create group</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
