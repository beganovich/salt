@extends('layouts.dashboard')
@section('title', 'Dashboard')

@section('content')

    <div class="container">

        <div id="groups">
            @include('dashboard.partials.groups')
        </div>

        <div id="users">
            @include('dashboard.partials.users')
        </div>

        <div id="roles">
            @include('dashboard.partials.roles')
        </div>
        
        <div id="settings">
            @include('dashboard.partials.settings')
        </div>

    </div>

@endsection