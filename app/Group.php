<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    //
    protected $fillable = ['name', 'description', 'creatorId'];

    public function member()  {
        return $this->hasMany('App\Member', 'group_id');
    }

    public function role() {
        return $this->hasMany('App\Role', 'group_id');
    }
}
