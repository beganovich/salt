<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function role() {
        return $this->hasOne('App\MemberRole');
    }

    protected $fillable = ['id', 'group_id'];
}
