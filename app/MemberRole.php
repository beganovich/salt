<?php

namespace App;

use App\MemberRole;
use Illuminate\Database\Eloquent\Model;

class MemberRole extends Model
{
    //
    protected $primaryKey = 'member_id';
    protected $fillable = ['member_id', 'role_id', 'group_id'];
    public $timestamps = false;
}
