<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function __construct() {
        $this->middleware(['guest']);
    }

    // Display register form.
    public function index() {
        return view('authentication.register');
    }

    // Saving user. 
    public function store(Request $request) {

        // Validate user's data: 
        $request->validate([
            'name' => 'required', 
            'email' => 'required|unique:users',
            'phone' => 'required|unique:users',
            'password' => 'required|confirmed'
        ]);

        // Save user to database. 
        User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'password' => Hash::make($request->input('password'))
        ]);

        // Log in user. 
        if(Auth::attempt($request->only('email', 'password'))) {
            return redirect()->route('user.dashboard');
        }
    }

}
