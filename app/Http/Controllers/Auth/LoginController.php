<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function __construct() {
    	$this->middleware(['guest']);
    }

    // Login form.
    public function index() {
    	return view('authentication.login');
    }

    // Attempt to login.
    public function attempt(Request $request) {

    	if(Auth::attempt($request->only('email', 'password'))) {
            return redirect()->route('user.dashboard');
        }

        // Flash session error (failed).
        Session::flash('error', 'Oops, looks like you missed something.');

        // Go back.
        return back();
    }

}
