<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LogoutController extends Controller
{
    // Logout.
    public function logout() {
        Auth::logout();
        return redirect()->route('login.form');
    }
}
