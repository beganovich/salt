<?php

namespace App\Http\Controllers\User;

use App\Role;
use App\Member;
use App\MemberRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{

    public function __construct()
    {
        return $this->middleware(['checksession']);
    }

    //
    public function edit(Request $request)
    {

        $request->validate([
            'role' => 'required',
            'value' => 'required|regex:/(^[A-Za-z0-9 ]+$)+/',
        ]);

        Role::where('group_id', Auth::user()->group->id)
            ->where('name', $request->input('role'))
            ->update(['name' => $request->input('value')]);

        MemberRole::where('group_id', Auth::user()->group->id)
            ->where('role_id', $request->input('role'))
            ->update(['role_id' => $request->input('value')]);

        return back();
    }

    public function create(Request $request)
    {

        $request->validate([
            'name' => 'required|unique:roles'
        ]);

        Role::create([
            'name' => $request->input('name'),
            'group_id' => Auth::user()->group->id
        ]);

        return back();
    }

    public function delete(Request $request, $name) {

        Role::where('group_id', Auth::user()->group->id)
            ->where('name', $name)
            ->delete();

        MemberRole::where('group_id', Auth::user()->group->id)
            ->where('role_id', $name)
            ->update(['role_id' => 'Not assigned']);

        return back();
    }
}
