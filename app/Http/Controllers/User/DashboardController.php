<?php

namespace App\Http\Controllers\User;

use App\Role;
use App\User;
use App\Group;
use App\MemberRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    //
    public function __construct() {
        $this->middleware(['checksession']);
    }

    public function index() {
        return view('dashboard.index');
    }
}
