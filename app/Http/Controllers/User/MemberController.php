<?php

namespace App\Http\Controllers\User;

use App\Role;
use App\User;
use App\MemberRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MemberController extends Controller
{

    public function __construct() {
        return $this->middleware(['checksession']);
    }
 
    //
    public function create(Request $request) {

        $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed'
        ]);

        User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone') || 'N/A',
            'password' => Hash::make($request->input('password')),
            'creator' => 0
        ]);

        return back();
    }

    public function role(Request $request) {

        $request->validate([
            'role' => 'required',
            'member' => 'required'
        ]);

        MemberRole::where('member_id', $request->input('member'))
            ->where('group_id', Auth::user()->group->id)
            ->update(['role_id' => $request->input('role')]);

        return back();
    }
}
