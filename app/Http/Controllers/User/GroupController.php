<?php

namespace App\Http\Controllers\User;

use App\Role;
use App\Group;
use App\Member;
use App\MemberRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{

    public function __construct() {
        $this->middleware(['checksession']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'group_name' => 'required',
            'group_description' => 'required'
        ]);

        Group::create([
            'name' => $request->input('group_name'),
            'description' => $request->input('group_description'),
            'creatorId' => Auth::id()
        ]);

        Member::create([
            'id' => Auth::id(),
            'group_id' => Auth::user()->group->id,
        ]);
        
        Role::create([
            'name' => 'Default',
            'group_id' => Auth::user()->group->id
        ]);

        MemberRole::create([
            'member_id' => Auth::id(),
            'role_id' => 'Default',
            'group_id' => Auth::user()->group->id
        ]);

        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
