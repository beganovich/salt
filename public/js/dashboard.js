let elem = document.querySelector('.tabs');
let options = {}

let instance = M.Tabs.init(elem, options);

$(document).ready(function () {
    $('.sidenav').sidenav();
    $('.modal').modal();
});

let sideNavButton = document.getElementById('sidebtn')
sideNavButton.addEventListener('click', () => {
    document.getElementById('trigger').click()
})

$('.roles').on('change', function () {
    window.location = $(this).val();
});

$(document).ready(function () {
    $('select').formSelect();
});
